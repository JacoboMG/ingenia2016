// XML.cpp: define el punto de entrada de la aplicaci�n de consola.
//

#include "stdafx.h"

using namespace rapidxml;
using namespace std;

int main()
{
	FILE * pFile;
	long lSize;
	char * buffer;
	size_t result;

	pFile = fopen("XMLFile.xml", "rb");  //_CRT_SECURE_NO_WARNINGS
	if (pFile == NULL) { fputs("File error", stderr); exit(1); }

	// obtener tama�o del archivo
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
	rewind(pFile);

	buffer = (char*)malloc(sizeof(char)*lSize + 1);
	if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }
	buffer[lSize] = 0;

	result = fread(buffer, 1, lSize, pFile);
	if (result != lSize) { fputs("Reading error", stderr); exit(3); }

	fclose(pFile);


	xml_document<> doc;    // character type defaults to char
	doc.parse<0>(buffer);    // 0 means default parse flags

	cout << "Name of my first node is: " << doc.first_node()->name() << "\n";
	xml_node<> *node = doc.first_node();
	node = node->first_node();
	xml_attribute<> *attr1 = node->first_attribute();
	cout << "Node: " << node->name() << " has attribute " << attr1->name() << "\n";
	node = node->first_node();
	for (xml_attribute<> *attr = node->first_attribute();
		attr; attr = attr->next_attribute())
	{
		cout << "Node: " << node->name() <<" has attribute " << attr->name() << " ";
		cout << "with value " << attr->value() << "\n";
	}

	free(buffer);

	system("pause");
	return 0;
}